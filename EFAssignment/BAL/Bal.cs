﻿using BusinessObject;
using DAL;

namespace BAL
{
    public class Bal
    {
        Dal dal = new Dal();
        public int AddProduct(Product product)
        {
            dal.AddProduct(product);
            return 1;
        }
        public Role GetUserRole(int roleName)
        {
            return dal.GetUserRole(roleName);
        }
        public int DeleteProduct(int id)
        {
            dal.DeleteProduct(id);
            return 1;
        }

        //public int EditProductName(int id, string name)
        //{
        //    dal.EditProductName(id, name);
        //    return 1;
        //}
        //public int EditProductDescription(int id, string description)
        //{
        //    dal.EditProductDescription(id, description);
        //    return 1;
        //}

        //public int EditProductQuantity(int id, int quantity)
        //{
        //    dal.EditProductQuantity(id, quantity);
        //    return 1;
        //}

        public int UpdateProduct(string name, Product product)
        {
            dal.UpdateProduct(name, product);
            return 1;
        }
        public int AddUser(User user)
        {
            dal.AddUser(user);
            return 1;
        }
        public int DeleteUser(int id)
        {
            dal.DeleteUser(id);
            return 1;
        }
        public int AddOrder(Order order)
        {
            dal.AddOrder(order);
            return 1;
        }
        public User GetUserDetails(string userName)
        {
            return dal.GetUserDetails(userName);
        }
        public List<Product> ViewProducts()
        {
            return dal.ViewProducts();
            
        }
        public Product GetProductDetails(string pName)
        {
            return dal.GetProductDetails(pName);
        }
    }
}