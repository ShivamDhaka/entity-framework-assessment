﻿using BAL;
using BusinessObject;

namespace EFAssignment
{
    class Program
    {
        public static void Main(string[] args)
        {
            Bal bal = new Bal();

            Console.WriteLine("Who are you? \n1.Admin\n2.Supplier\n3.Customer\n");
            int ch = Convert.ToInt32(Console.ReadLine());
           
            switch (ch)
            {
                case 1:
                    {
                        Console.WriteLine("1.Add User\n2.Delete User");
                        int choice = Convert.ToInt32(Console.ReadLine());
                        if(choice == 1)
                        {

                            // ADD USER

                            Console.WriteLine( "If you want to add Supplier then press 2 or If you want to add Customer then press 3");
                            int userType= Byte.Parse(Console.ReadLine());
                            Role userRole = bal.GetUserRole(userType);
                            Console.WriteLine("Enter First Name:");
                            string fName = Console.ReadLine();
                            Console.WriteLine("Enter Last Name");
                            string lName = Console.ReadLine();
                            Console.WriteLine("Enter Email:");
                            string email = Console.ReadLine();
                            User user = new User()

                            {
                                FirstName = fName,
                                LastName = lName,
                                Email = email,
                                DateCreated = DateTime.Now,
                                IsActive = true,
                                role = userRole
                            };
                            int response = bal.AddUser(user);
                            Console.WriteLine("User has been added!");
                            Console.WriteLine(response);
                        }
                        else if(choice == 2)
                        {

                            // DELETE USER

                            Console.WriteLine("Enter ID of the User you want to Delete(Soft):");
                            int uId = Convert.ToInt32(Console.ReadLine());
                            int resUser = bal.DeleteUser(uId);
                            Console.WriteLine("User has been deleted(Softly)!");
                            Console.WriteLine(resUser);
                        }
                        else
                        {
                            Console.WriteLine("Invalid Option");
                        }
                        break;
                    }
                case 2: 
                    {
                        Console.WriteLine("1.Add Product\n2.Delete Product\n3.Edit Product\n");
                        int choice = Convert.ToInt32(Console.ReadLine());
                        if (choice == 1)
                        {

                            //ADD PRODUCT 

                            Console.WriteLine("Enter Product Name:");
                            string prodName = Console.ReadLine();
                            Console.WriteLine("Enter Description:");
                            string description = Console.ReadLine();
                            Console.WriteLine("Enter QtyInStock:");
                            int qty = Convert.ToByte(Console.ReadLine());
                            Product product = new Product()
                            {
                                ProductName = prodName,
                                Description = description,
                                DateCreated = DateTime.Now,
                                QtyInStock = qty
                            };
                            int res = bal.AddProduct(product);
                            Console.WriteLine("Product has been added!");
                            Console.WriteLine(res);
                        }
                        else if(choice == 2)
                        {

                            //DELETE PRODUCT

                            Console.WriteLine("Enter the id of the product which you want to DELETE:");
                            int prodId = Convert.ToInt32(Console.ReadLine());
                            int resObj = bal.DeleteProduct(prodId);
                            Console.WriteLine("Product has been deleted!");
                            Console.WriteLine(resObj);
                        }
                        else if(choice ==3)
                        {
                            //EDIT PRODUCT

                            //Console.WriteLine("Enter the id of the product which you want to EDIT:");
                            //int pId = Convert.ToInt32(Console.ReadLine());
                            //Console.WriteLine("Which property you want to edit?\n1.Product Name.\n2.Product Description.\n3.Quantity in Stock.");
                            //int c = Convert.ToInt32(Console.ReadLine());
                            //if(c == 1)
                            //{
                            //    //UPDATE PRODUCT NAME

                            //    Console.WriteLine("Enter the updated product name:");
                            //    string name = Console.ReadLine();
                            //    int prodNameUpd = bal.EditProductName(pId, name);
                            //    Console.WriteLine("Product Name Updated!");
                            //    Console.WriteLine(prodNameUpd);
                            //}
                            //else if(c == 2)
                            //{

                            //    //UPDATE PRODUCT DESCRIPTION

                            //    Console.WriteLine("Enter the updated product description:");
                            //    string description = Console.ReadLine();
                            //    int prodDescUpd = bal.EditProductDescription(pId, description);
                            //    Console.WriteLine("Product description Updated!");
                            //    Console.WriteLine(prodDescUpd);
                            //}
                            //else if(c == 3)
                            //{

                            //    //UPDATE PRODUCT QUANTITY

                            //    Console.WriteLine("Enter the Updated Quantity:");
                            //    int quantity = Convert.ToInt32(Console.ReadLine());
                            //    int prodQtyUpd = bal.EditProductQuantity(pId, quantity);
                            //    Console.WriteLine("Product Quantity Updated!");
                            //    Console.WriteLine(prodQtyUpd);
                            //}
                            //else
                            //{
                            //    Console.WriteLine("Enter Valid Option");
                            //}

                            Console.WriteLine("Enter the name of the product to edit");
                            string name = Console.ReadLine();
                            Product product = bal.GetProductDetails(name);
                            if (product != null)
                            {
                                Console.WriteLine("Enter the new name");
                                string? ProductName = Console.ReadLine();
                                Console.WriteLine("Enter the new Description");
                                //int Price = Convert.ToInt32(Console.ReadLine());
                                string? description = (Console.ReadLine());
                                Console.WriteLine("Enter the new Quantity");
                                int Quantity;
                                Int32.TryParse(Console.ReadLine(), out Quantity);
                                Product upDatedProduct = new Product()
                                {
                                    ProductName = ProductName,
                                    Description = description,
                                    QtyInStock = Quantity
                                };
                                bal.UpdateProduct(name, upDatedProduct);
                                Console.WriteLine("It has been updated");
                            }


                        }
                        else
                        {
                            Console.WriteLine("Invalid Option");
                        }
                        break;
                    }
                case 3:
                    {
                        // CUSTOMER ROLE

                        Console.WriteLine("1.View Products.\n2.Place Order.");
                        int c = Convert.ToInt32(Console.ReadLine());
                        if(c == 1)
                        {

                            //VIEW PRODUCTS
                            Console.WriteLine("ALL Orders:");
                            List<Product> list = bal.ViewProducts();
                            foreach(Product product in list)
                            {
                                Console.WriteLine("Product Name =" + product.ProductName + ", Product Description = " + product.Description + ", Product Quantity = " + product.QtyInStock);
                            }
                        }
                        else if(c==2)
                        {

                            //PLACE ORDER

                            Console.WriteLine("Enter the Product Name which you want to place order!");
                            string pName = Console.ReadLine();
                            Product prod = bal.GetProductDetails(pName);
                            Console.WriteLine("Enter your name");
                            string userName = Console.ReadLine();

                            User user = bal.GetUserDetails(userName);
                            Order order = new Order()
                            {
                               
                                OrderDate = DateTime.Now,
                                product = prod,
                                user = user,

                            };
                            int resOrder = bal.AddOrder(order);
                            Console.WriteLine("Order has been placed!");
                            Console.WriteLine(resOrder);
                        }
                        else
                        {
                            Console.WriteLine("Invalid Option!");
                        }
                        break;
                    }
            }
        }
    }
}