﻿using BusinessObject;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Context
{
    public class EcommerceDbContext : DbContext
    {
        public EcommerceDbContext()
        {

        }
        public EcommerceDbContext(DbContextOptions<EcommerceDbContext> options)
            : base(options)
        {

        }
        public DbSet<User> users { get; set; }
        public DbSet<Role> roles { get; set; }
        public DbSet<Order> orders { get; set; }
        public DbSet<Product> products { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"data source=INW-923;initial catalog=EcommerceDb;integrated security=true;TrustServerCertificate=True");
        }
    }
}
