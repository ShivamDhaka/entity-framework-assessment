﻿using DAL.Context;
using BusinessObject;

namespace DAL
{
    public class Dal
    {
        EcommerceDbContext db = new EcommerceDbContext();
        public int AddProduct(Product product)
        {
            db.products.Add(product);
            db.SaveChanges();
            return 1;
        }

        public Role GetUserRole(int roleName)
        {
            return db.roles.FirstOrDefault(x => x.Id == roleName);
        }

        public int DeleteProduct(int id)
        {
            Product p = db.products.Where(x => x.ProductId == id).FirstOrDefault();
            if(p != null)
            {
                db.products.Remove(p);
                db.SaveChanges();
            }
            else
            {
                Console.WriteLine("There is no record");
            }
            return 1;
        }

        //public int EditProductName(int id, string name)
        //{
        //    Product pObj = db.products.Where(x => x.ProductId == id).FirstOrDefault();
        //    if(pObj != null )
        //    {
        //        foreach (Product temp in db.products)
        //        {
        //            if(temp.ProductId == pObj.ProductId)
        //            {
        //                temp.ProductName = name;
        //            }
        //        }
        //        db.SaveChanges();
        //    }
        //    else
        //    {
        //        Console.WriteLine("There is no record");
        //    }
        //    return 1;
        //}
        //public int EditProductQuantity(int id, int quantity)
        //{
        //    Product pObj = db.products.Where(x => x.ProductId == id).FirstOrDefault();
        //    if (pObj != null)
        //    {
        //        foreach (Product temp in db.products)
        //        {
        //            if (temp.ProductId == pObj.ProductId)
        //            {
        //                temp.QtyInStock = quantity;
        //            }
        //        }
        //        db.SaveChanges();
        //    }
        //    else
        //    {
        //        Console.WriteLine("There is no record");
        //    }
        //    return 1;
        //}
        //public int EditProductDescription(int id, string description)
        //{
        //    Product pObj = db.products.Where(x => x.ProductId == id).FirstOrDefault();
        //    if (pObj != null)
        //    {
        //        foreach (Product temp in db.products)
        //        {
        //            if (temp.ProductId == pObj.ProductId)
        //            {
        //                temp.Description = description;
        //            }
        //        }
        //        db.SaveChanges();
        //    }
        //    else
        //    {
        //        Console.WriteLine("There is no record");
        //    }
        //    return 1;
        //}

        public int UpdateProduct(string name, Product product)
        {
            Product obj = db.products.Where(x => x.ProductName == name).FirstOrDefault();
            if (obj != null)
            {
                if (product.ProductName.Length > 0)
                    obj.ProductName = product.ProductName;

                if (product.Description.Length > 0)
                    obj.Description = product.Description;

                if (product.QtyInStock > 0)
                    obj.QtyInStock = product.QtyInStock;

                db.products.Update(obj);
                db.SaveChanges();
            }
            else
            {
                return 1;
            }

            return 0;

        }

        public User GetUserDetails(string userName)
        {
            return db.users.FirstOrDefault(x => x.FirstName == userName && x.role.Id == 3);
        }

        public int AddUser(User user)
        {
            db.users.Add(user);
            db.SaveChanges();
            return 1;
        }
        public int DeleteUser(int id)
        {
            User u = db.users.Where(x=>x.UserId == id).FirstOrDefault();
            if (u != null)
            {
                foreach(User temp in db.users)
                {
                    if(temp.UserId == u.UserId)
                    {
                        temp.IsActive = false;
                    }
                }
                db.SaveChanges();
            }
            else
            {
                Console.WriteLine("There is no record of this user!");
            }
            return 1;
        }
        public int AddOrder(Order order)
        {
            db.orders.Add(order);
            db.SaveChanges();
            return 1;
        }

        public List<Product> ViewProducts()
        {
            List<Product> list = db.products.ToList();
            return list;
        }

        public Product GetProductDetails(string pName)
        {
            return db.products.FirstOrDefault(x => x.ProductName == pName);
        }
    }
}